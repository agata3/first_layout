import { Home } from "./pages/Home";
import { Technology } from "./pages/Technology";
import { Design } from "./pages/Design";
import { createMuiTheme } from "@material-ui/core/styles";
import { JoinUs } from "./pages/JoinUs";
import { Health } from "./pages/Health";
import { Style } from "./pages/Style";
import { Travel } from "./pages/Travel";

export const pages = [
  { title: "Home", component: <Home /> },
  { title: "Technology", component: <Technology /> },
  { title: "Design", component: <Design /> },
  { title: "Join Us", component: <JoinUs /> },
  //   { title: "Business" },
  //   { title: "Politics" },
  //   { title: "Opinion" },
  //   { title: "Science" },
  { title: "Health", component: <Health /> },
  { title: "Style", component: <Style /> },
  { title: "Travel", component: <Travel /> },
];

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#5216ac",
    },
    secondary: {
      main: "#bf360c",
    },
  },
  typography: {
    fontFamily: "Comfortaa",
  },
  overrides: {
    // Style sheet name ⚛️
    MuiButton: {
      // Name of the rule
      hover: {
        // Some CSS
        color: "white",
      },
    },
  },
});
theme.overrides = {
  MuiPaper: {
    root: {
      background:
        "linear-gradient(to right, #aea0d5, #eaafc8)" /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */,
      boxShadow: "0 0 20px 0 #f5005780",
      animation: "mui-ripple-pulsate 1s infinite",
    },
  },
  MuiContainer: {
    root: {},
  },
};
