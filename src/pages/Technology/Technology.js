import React from "react";
import { Box } from "@material-ui/core";
import { Main } from "./Main";
import { TileDataPicture } from "./TileDataPicture";

import { Copyright } from "../../components/Copyright";

export const Technology = () => {
  return (
    <Box>
      <TileDataPicture />
      <Main />
      <Copyright />
    </Box>
  );
};
