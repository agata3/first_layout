import React from "react";
import { LinePicture } from "./LinePicture";
import { useState } from "react";

export const TileDataPicture = () => {
  const [list] = useState([
    {
      key: 1,
      title: "news",
      nameSecondary: "July 20, 2016",
      image: "http://placekitten.com/200/200?image=12",
    },
    {
      key: 2,
      title: "photos",
      nameSecondary: "May 20, 2014",
      image: "http://placekitten.com/200/200?image=10",
    },
    {
      key: 3,
      title: "about",
      nameSecondary: "July 20, 2014",
      image: "http://placekitten.com/200/200?image=1",
    },
    {
      key: 4,
      title: "news",
      nameSecondary: "Jun 20, 2015",
      image: "http://placekitten.com/200/200?image=3",
    },
    {
      key: 5,
      title: "news",
      nameSecondary: "July 20, 2019",
      image: "http://placekitten.com/200/200?image=8",
    },
    {
      key: 6,
      title: "news",
      nameSecondary: "July 20, 2020",
      image: "http://placekitten.com/200/200",
    },
  ]);
  return <LinePicture list={list}></LinePicture>;
};
