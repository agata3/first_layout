import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import { Grid, Box, Typography } from "@material-ui/core";
import picture from "./picture.jpg";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import { purple } from "@material-ui/core/colors";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import StarIcon from "@material-ui/icons/Star";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  gridList: {
    borderRadius: 5,
    overflow: "cover",
  },
  medical: {
    backgroundColor: "#fce4ec",
    paddingLeft: 5,

    borderRadius: 5,
  },
}));

export const MainHealth = () => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Typography component="div" className={classes.medical}>
        <Box
          textAlign="flex-start"
          pt={2}
          mt={1}
          fontWeight="fontWeightLight"
          fontSize={10}
        >
          Volume 29 November 2
        </Box>
        <Box textAlign="justify" mb={1} fontSize={13}>
          March 2021
        </Box>
        <Box
          textAlign="center"
          mt={1}
          mb={-2}
          pb={2}
          fontSize={26}
          fontWeight="fontWeightBold"
        >
          Medical Writing
        </Box>
      </Typography>
      <Grid
        container
        spacing={3}
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12}>
          <Box>
            <GridList className={classes.gridList} cols={1} cellHeight={260}>
              <img
                src={picture}
                alt="medical"
                className={classes.gridList}
              ></img>
            </GridList>
          </Box>
        </Grid>
      </Grid>
      <Typography component="div" className={classes.medical}>
        <Box
          textAlign="center"
          mb={1}
          mt={-2}
          pt={2}
          fontSize={20}
          fontWeight="fontWeightBold"
        >
          Visual Communications
        </Box>
        <Box
          textAlign="flex-start"
          pt={2}
          fontWeight="fontWeightBold"
          fontSize={10}
        >
          also in this issues...
        </Box>
        <Box textAlign="justify" mb={1} fontSize={12} pb={2} pr={8} pl={1}>
          However each can also be limited to only updating once per day or
          week. To do so simply
        </Box>
      </Typography>
      <List component="nav" aria-label="contacts">
        <ListItem>
          <ListItemIcon>
            <StarIcon style={{ color: purple[100] }} fontSize="small" />
          </ListItemIcon>
          <Box fontSize={12} fontWeight="fontWeightLight">
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
          </Box>
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <StarIcon style={{ color: purple[200] }} fontSize="small" />
          </ListItemIcon>
          <Box fontSize={12} fontWeight="fontWeightLight">
            {" "}
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
          </Box>
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <StarIcon style={{ color: purple[300] }} fontSize="small" />
          </ListItemIcon>
          <Box fontSize={12} fontWeight="fontWeightLight">
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
          </Box>
        </ListItem>
      </List>
    </Box>
  );
};
