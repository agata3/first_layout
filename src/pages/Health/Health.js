import React from "react";
import { Copyright } from "../../components/Copyright";
import { MainHealth } from "./MainHealth";

export const Health = () => {
  return (
    <>
      <MainHealth />
      <Copyright />
    </>
  );
};
