import React from "react";
import { Box, IconButton } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import zd from "./zd.jpg";
import { makeStyles } from "@material-ui/core/styles";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import { Copyright } from "../../components/Copyright";
import { TravelThree } from "./TravelThree";

const useStyles = makeStyles({
  root: {
    // maxWidth: 345,
    backgroundColor: "#f3e5f5",
    borderRadius: 5,
  },
  media: {
    height: 140,
    borderRadius: 5,
  },
  root2: {
    // maxWidth: 345,
    backgroundColor: "#f3e5f5",
    borderRadius: 2,
    marginTop: 20,
    marginBottom: 30,
    padding: 0,
  },
  media2: {
    height: 200,
    borderRadius: 3,
  },

  facebook: {
    position: "fixed",
    left: 1,
    zIndex: 1,
    top: 90,
  },
});

export const MainTravel = () => {
  const classes = useStyles();
  return (
    <div>
      <Box className={classes.facebook}>
        <IconButton edge="start">
          <img
            src="https://d6tizftlrpuof.cloudfront.net/themes/production/virginholidays-virginholidays-2017-06-21-button-fae3f50b3dfeb119391ecb7b3a171be1.png"
            width="35"
            height="110"
            alt="facebook"
          ></img>
        </IconButton>
      </Box>
      <Box position="relative" display="flex">
        <Container
          m={1}
          p={1}
          component="div"
          style={{
            backgroundImage: `url(${zd})`,
            backgroundSize: "cover",
            height: "100vh",

            backgroundPositionX: -200,
            borderRadius: 5,
            position: "absolute",
          }}
        >
          <Container
            maxWidth="s"
            style={{
              backgroundImage: `url(${zd})`,
              backgroundSize: "cover",
              height: "100vh",
              width: "80vw",
              backgroundPositionX: -240,
              filter: "blur(4px)",
              position: "absolute",
            }}
          ></Container>
          <Typography component="div">
            <Box className={classes.root} m={2}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image="https://source.unsplash.com/user/erondu/daily"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Our flexible no change fees policy
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    To give you greater peace of mind with your upcoming travel
                    plans, we won't charge for changes to your holiday. That
                    includes changing your travel dates or even your holiday
                    destination
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Find out more
                </Button>
                <Button size="small" color="primary">
                  Spread the cost
                </Button>
              </CardActions>
            </Box>
          </Typography>
          <Box>
            <Typography component="div">
              <Box position="relative" p={2}>
                <Box
                  textAlign="center"
                  fontSize={18}
                  fontWeight="bold"
                  lineHeight={2}
                >
                  Our flexible no change fees policy
                </Box>
                <Box fontSize={14} m={1}>
                  We understand that you may wish to change your holiday plans
                  and we appreciate the need for flexibility.
                </Box>
                <Box fontSize={14} m={1}>
                  {" "}
                  To give you greater peace of mind with your upcoming travel
                  plans.
                </Box>
                <Box fontSize={14} m={1}>
                  {" "}
                  That includes changing your{" "}
                  <Box fontSize={14} m={1}>
                    {" "}
                    That includes changing your travel dates or even your
                    holiday destination.
                  </Box>{" "}
                </Box>
              </Box>
            </Typography>
          </Box>
          <TravelThree />
          <Box style={{ paddingBottom: 10 }}>
            <Copyright />
          </Box>
        </Container>
      </Box>
    </div>
  );
};
