export const lists = [
  {
    key: 1,
    title: "  Maledives holidays",
    price: " $ 1449pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2Fpersonalisation%2Fmaldives.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
  {
    key: 2,
    title: "Barbados holidays",
    price: " $ 899pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2Fhomepage-promo-pods-sale-lockups%2Fweb-peak-collateral_core_promo-pod_barbados_no-text.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
  {
    key: 3,
    title: "  Antigua",
    price: " $ 1199pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-carousel%2F2316x696-VH-Desktop-Homepage_Peace-of-mind-%28004%29.jpg&amp;format=jpg",
  },
  {
    key: 4,
    title: " Orlando weekend",
    price: " $ 449pp",
    backgroundImage:
      "//d3hk78fplavsbl.cloudfront.net/assets/common-prod/hotel/205/27679/27679-1-results_carousel.jpg?version=40",
  },
  {
    key: 5,
    title: "New York city break",
    price: " $ 849pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2FNYC.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
];
