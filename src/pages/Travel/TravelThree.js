import React from "react";
import { Box, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import IconButton from "@material-ui/core/IconButton";
import { lists } from "./TravelList";

const useStyles = makeStyles({
  root: {
    position: "relative",
    top: -20,
    borderTop: 1,
    paddingTop: 4,
    marginBottom: 2,
  },
});

export const TravelThree = () => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      {lists.map((list) => (
        <>
          <Box
            mt={5}
            mb={5}
            borderTop={1}
            borderColor="grey.500"
            key={list.key}
          ></Box>
          <Grid container justify="center">
            <Grid
              item
              xs={12}
              maxWidth="s"
              style={{
                backgroundImage: `url(${list.backgroundImage})`,
                backgroundSize: "cover",
                height: 200,
                borderRadius: 2,
              }}
            ></Grid>
          </Grid>
          <Box display="flex" mt={1} style={{ width: "100%" }}>
            <Typography component="div">
              <Box
                fontWeight="fontWeightBold"
                fontSize={18}
                textAlign="left"
                style={{ width: "100%" }}
              >
                {list.title}
              </Box>{" "}
            </Typography>{" "}
          </Box>
          <div style={{ width: "100%" }}>
            <Typography component="div">
              <Box display="flex" alignItems="center">
                <Box fontSize={22} fontWeight={800} flexGrow={1}>
                  Prices from{" "}
                  <strong
                    style={{ color: "red", fontWeight: "bolder", fontSize: 22 }}
                  >
                    {list.price}
                  </strong>
                </Box>
                <Box>
                  {" "}
                  <IconButton style={{ color: "red" }}>
                    {<ArrowForwardIosIcon fontSize="large" />}
                  </IconButton>
                </Box>
              </Box>
              <Box>
                <img
                  src="//www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-11619-5.svg"
                  alt="Trip advisor rating: 4.5"
                  aria-hidden="true"
                ></img>
              </Box>
            </Typography>{" "}
          </div>
        </>
      ))}
    </Box>
  );
};
