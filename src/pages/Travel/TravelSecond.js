import React from "react";
import { Box } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { TravelThree } from "./TravelThree";
import zd from "./zd.jpg";
import { makeStyles } from "@material-ui/core/styles";
import CardActionArea from "@material-ui/core/CardActionArea";

import CardMedia from "@material-ui/core/CardMedia";

const useStyles = makeStyles({
  media1: {
    height: 340,
    width: 180,
    "&:hover": {
      cursor: "pointer",
      transition: "all 2s linear",
      transform: "rotateY(360deg)",
      borderRadius: 5,
    },
  },
  relative: {
    left: 0,
    right: 50,
    "&:hover": {},
  },
});

export const TravelSecond = () => {
  const classes = useStyles();
  return (
    <div>
      <Box
        mt={2}
        className={classes.relative}
        position="relative"
        style={
          {
            // transform: "rotate(45deg)",
            // transform: "scale(2)",
            // transform: "skewX(10deg)",
            // transform: "translate(-20px)",
          }
        }
      >
        <Box>
          {" "}
          <CardActionArea>
            <CardMedia
              style={{
                backgroundImage: `url(${zd})`,
                backgroundSize: "cover",
                backgroundPositionY: -60,
                backgroundPositionX: -300,
              }}
              className={classes.media1}
            />
          </CardActionArea>
        </Box>

        <Box m={1} display="flex" position="absolute" top={10}>
          <Typography variant="body1">
            <Box fontWeight="fontWeightBold" m={1} textAlign="right">
              {" "}
              To give you greater peace of mind with your upcoming travel plans.
              That includes changing your travel dates or even your holiday
              destination.
            </Box>

            <Box mt={1} fontWeight="fontWeightBold" textAlign="right">
              {" "}
              We understand that you may wish to change your holiday plans
              <span>and we appreciate </span>
              the need for flexibility.
            </Box>
          </Typography>
        </Box>
        <TravelThree></TravelThree>
      </Box>
    </div>
  );
};
