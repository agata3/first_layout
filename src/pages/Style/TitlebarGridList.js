import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import { Box, IconButton } from "@material-ui/core";
import InfoIcon from "@material-ui/icons/Info";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    borderRadius: 13,
    background:
      "linear-gradient(to right, #aea0d5, #eaafc8)" /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */,
    boxShadow: "0 0 20px 0 #f5005780",
    animation: "mui-ripple-pulsate 1s infinite",
  },
  gridList: {
    width: 500,
    height: 450,
    borderRadius: 3,
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

export const TitlebarGridList = ({ list }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root} mt={1}>
      <Box fontSize="h6.fontSize" m={1}>
        December
      </Box>
      <GridList
        cellHeight={160}
        className={classes.gridList}
        cols={3}
        style={{ height: "auto" }}
        spacing={8}
      >
        {list.map((tile) => (
          <GridListTile key={tile.image} cols={tile.cols || 1}>
            <img src={tile.image} alt={tile.title} />
            <GridListTileBar
              key={tile.key}
              title={tile.title}
              subtitle={<span>by: {tile.author}</span>}
              actionIcon={
                <IconButton
                  aria-label={`info about ${tile.title}`}
                  className={classes.icon}
                >
                  <InfoIcon />
                </IconButton>
              }
            />
          </GridListTile>
        ))}{" "}
      </GridList>
    </Box>
  );
};
