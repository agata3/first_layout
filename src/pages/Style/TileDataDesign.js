import React from "react";
import { TitlebarGridList } from "./TitlebarGridList";
import { useState } from "react";

export const TileDataDesign = () => {
  const [list] = useState([
    {
      key: 1,
      title: "news",
      image: "https://picsum.photos/200/300.jpg",
      author: "Agatha Christie",
      cols: 3,
    },
    {
      key: 2,
      title: "photos",
      image: "https://picsum.photos/id/237/200/300",
      author: "Mary Ann Evans — AKA George Eliot",
      cols: 1,
    },
    {
      key: 3,
      title: "about",
      image: "https://source.unsplash.com/collection/190727/200x200",
      author: "Currer, Ellis, and Acton Bell",
      cols: 1,
    },
    {
      key: 4,
      title: "news",
      image: "https://picsum.photos/200/300?grayscale",
      author: "Mary Ann Evans",
      cols: 1,
    },
    {
      key: 5,
      title: "news",
      image: "http://placekitten.com/200/200?image=8",
      author: "Mary Ann Evans — AKA George Eliot",
      cols: 2,
    },
    {
      key: 6,
      title: "news",
      image: "https://source.unsplash.com/user/erondu/daily",
      author: "Agatha Christie",
      cols: 1,
    },
    {
      key: 7,
      title: "news",
      image: "http://placekitten.com/200/200",
      author: "Eric Blair",
      cols: 1,
    },
    {
      key: 8,
      title: "news",
      image: "https://source.unsplash.com/1600x900/?nature,water",
      author: " AKA George Eliot",
      cols: 2,
    },
    {
      key: 9,
      title: "news",
      image: "https://picsum.photos/200/300?random=14",
      author: "Eric Blair",
      cols: 3,
    },
    {
      key: 10,
      title: "news",
      image: "https://picsum.photos/200/300?random=3",
      author: " AKA George Eliot",
      cols: 2,
    },
    {
      key: 11,
      title: "news",
      image: "https://picsum.photos/200/300?random=4",
      author: " AKA George Eliot",
      cols: 1,
    },
  ]);
  return <TitlebarGridList list={list}></TitlebarGridList>;
};
