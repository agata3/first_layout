import React from "react";
import { TileDataDesign } from "./TileDataDesign";
import { Copyright } from "../../components/Copyright";
import { Box } from "@material-ui/core";

export const Style = () => {
  return (
    <Box>
      <TileDataDesign />
      <Copyright />
    </Box>
  );
};
