import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: "#fce4ec",
  },
  gridList: {
    width: 250,
  },
  cat: {
    overflow: "cover",
    padding: theme.spacing(1),
  },
  catBox: {
    flexWrap: "wrap",
    display: "flex",
    justifyContent: "space-around",
  },
  div: {
    backgroundColor:
      "#fce4ec" /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */,
    marginBottom: 0,
    borderRadius: 5,
  },
  picture: {
    borderRadius: 5,
  },
  pictureMain: {
    borderRadius: 5,
    boxShadow: "0 0 15px 0 #f5005780",
    animation: "mui-ripple-pulsate 1s infinite",
  },
  pictureMain1: {
    borderRadius: 5,
    backgroundColor: "0 0 30px 0 #f5005780",
    animation: "mui-ripple-pulsate 1s infinite",
    paddingBottom: 15,
  },
}));

export const MainDesign = () => {
  const classes = useStyles();

  return (
    <div className={classes.div}>
      <Box className={classes.root} style={{ width: "100%" }}>
        <Box
          display="flex"
          justifyContent="flex-start"
          m={1}
          p={1}
          borderRadius={16}
        >
          <Box>
            <img
              src={"http://placekitten.com/240/150?image=2"}
              alt="cat"
              className={classes.pictureMain}
            />
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-end" m={1} p={1}>
          <Box>
            <img
              src={"http://placekitten.com/230/170?image=1"}
              alt="cat"
              className={classes.pictureMain}
            />
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-start" m={1} p={1}>
          <Box>
            <img
              src={"https://source.unsplash.com/collection/190737/250x150"}
              alt="cat"
              className={classes.pictureMain}
            />
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-end" m={1} p={1}>
          <Box>
            <img
              src={"https://source.unsplash.com/collection/190727/240x150"}
              alt="cat"
              className={classes.pictureMain}
            />
          </Box>
        </Box>
      </Box>
      <Box className={classes.pictureMain1} orderRight={1}>
        <Box style={{ width: "100%" }}>
          <Box
            display="flex"
            alignItems="flex-start"
            mt={3}
            css={{ height: 100 }}
          >
            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=8"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat} alignSelf="Center">
              <img
                src={"http://placekitten.com/100/80?image=9"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=16"}
                alt="cat"
                className={classes.picture}
              />
            </Box>
          </Box>
        </Box>
        <Box style={{ width: "100%" }}>
          <Box
            display="flex"
            alignItems="flex-start"
            css={{ height: 100 }}
            mt={2}
          >
            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=14"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat} alignSelf="flex-end">
              <img
                src={"http://placekitten.com/100/100?image=15"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=2"}
                alt="cat"
                className={classes.picture}
              />
            </Box>
          </Box>
        </Box>
        <Box style={{ width: "100%" }}>
          <Box
            display="flex"
            alignItems="flex-start"
            mt={2}
            css={{ height: 100 }}
          >
            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=4"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat} alignSelf="Center">
              <img
                src={"http://placekitten.com/100/120?image=5"}
                alt="cat"
                className={classes.picture}
              />
            </Box>

            <Box className={classes.cat}>
              <img
                src={"http://placekitten.com/100/100?image=6"}
                alt="cat"
                className={classes.picture}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </div>
  );
};
