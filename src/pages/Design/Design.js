import React from "react";
import { Box } from "@material-ui/core";

import { Copyright } from "../../components/Copyright";
import { MainDesign } from "./MainDesign";

export const Design = () => {
  return (
    <Box>
      <MainDesign />
      <Copyright></Copyright>
    </Box>
  );
};
