import { Box } from "@material-ui/core";
import React from "react";
import BlogPost from "../components/BlogPost";
import { Footer } from "../components/Footer";
import PaperM from "../components/PaperM";
import Picture from "../components/Picture";
import Pictures from "../components/Pictures";

export const Home = () => {
  return (
    <Box>
      <Picture />
      <Pictures />
      <BlogPost />
      <PaperM />
      <Footer />
    </Box>
  );
};
