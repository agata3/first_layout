import { MuiThemeProvider } from "@material-ui/core/styles";
import { theme } from "../config";
import Header from "../components/Header";
import Container from "@material-ui/core/Container";
import { Route } from "react-router-dom";
import { pages } from "../config";

export const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Container maxWidth="lg">
        <Header />
        {pages.map((page) => (
          <Route
            exact
            path={`/${page.title === "Home" ? "" : page.title.toLowerCase()}`}
          >
            {page.component}
          </Route>
        ))}
      </Container>
    </MuiThemeProvider>
  );
};
