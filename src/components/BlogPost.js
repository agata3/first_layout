import React from "react";
import Sidenote from "./Sidenote";
import { useState } from "react";

const BlogPost = () => {
  const [blogs] = useState([
    {
      title: "Sample blog post",
      subtitle1: "April 1, 2020 by Olivier",
      subtitle2:
        "This blog post shows a few different types of content that are supported and styled with Material styles. Basic typography, images.",

      subtitle:
        "Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.",
      li1:
        "Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
      li2: "Donec id elit non mi porta gravida at eget metus.",
      li3: "Nulla vitae elit libero, a pharetra augue.",
      last:
        "Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue.",
    },
  ]);
  return <Sidenote blog={blogs}></Sidenote>;
};

export default BlogPost;
