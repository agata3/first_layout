import { Box, Typography, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",

    "& > *": {
      marginTop: theme.spacing(2),
      padding: theme.spacing(2),
    },
  },
  root1: {
    background: "#e0e0e0",
    boxShadow: "0 0 7px 0 #f5005780",

    animation: "mui-ripple-pulsate 1s infinite",
    borderRadius: 3,
  },
}));
const PaperM = () => {
  const classes = useStyles();
  return (
    <Box className={classes.root} mt={2}>
      <Container className={classes.root1}>
        <Box m={1}>
          {" "}
          <Typography variant="h6">About</Typography>
        </Box>

        <Typography>
          Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis
          ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id
          elit. Etiam porta sem malesuada magna mollis euismod. Cras mattis
          consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla
          sed consectetur.
        </Typography>
      </Container>
    </Box>
  );
};

export default PaperM;
