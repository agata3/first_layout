import React from "react";
import { Box, Typography } from "@material-ui/core";

export const Footer = () => {
  return (
    <Box
      display="flex"
      color="text.secondary"
      p={1}
      m={2}
      justifyContent="center"
    >
      <Typography>
        <Box textAlign="center" mt={4}>
          Something here to give the footer a purpose!{" "}
        </Box>
        <Box textAlign="center" fontSize={14}>
          Copyright © Your Webside 2021.
        </Box>
      </Typography>
    </Box>
  );
};
