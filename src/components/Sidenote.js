import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Box from "@material-ui/core/Box";
import { Typography } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import { purple } from "@material-ui/core/colors";

import StarIcon from "@material-ui/icons/Star";

export default function Sidenote({ blog }) {
  return (
    <Box mt={5}>
      <Typography component="div" variant="h6">
        <Box fontWeight={800}>From the firehose</Box>
      </Typography>
      <Box
        display="flex"
        bgcolor="background.paper"
        mt={2}
        borderTop={0.5}
        borderColor="grey.500"
      >
        <Box mt={3}>
          {blog.map((name) => (
            <Typography>
              <Typography variant="h5">{name.title}</Typography>
              <Box color="primary.main" mb={3}>
                <Typography fontStyle="oblique" variant="subtitle2">
                  {name.subtitle1}
                </Typography>
              </Box>

              <Typography variant="inherit">{name.subtitle2}</Typography>
              <Typography variant="inherit">{name.subtitle}</Typography>
              <List component="nav" aria-label="contacts">
                <ListItem>
                  <ListItemIcon>
                    <StarIcon style={{ color: purple[100] }} />
                  </ListItemIcon>
                  <ListItemText primary={name.li1} />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <StarIcon style={{ color: purple[200] }} />
                  </ListItemIcon>
                  <ListItemText primary={name.li2} />
                </ListItem>
                <ListItem>
                  <ListItemIcon>
                    <StarIcon style={{ color: purple[300] }} />
                  </ListItemIcon>
                  <ListItemText primary={name.li3} />
                </ListItem>
              </List>
              <Box mt={2}>
                {" "}
                <Typography>{name.last}</Typography>
              </Box>
            </Typography>
          ))}
        </Box>
      </Box>
    </Box>
  );
}
